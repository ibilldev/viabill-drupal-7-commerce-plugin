### DRUPAL COMMERCE  : ViaBill ePay Module ###
----------------------  

ViaBill has developed a free payment module using Epay Payment Gateway which enables your customers to pay online for their orders in your Drupal Commerce Web Shop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
---------
- version: 1.1
- Plugin on BitBucket (https://pdviabill@bitbucket.org/ibilldev/viabill-drupal-7-commerce-plugin.git)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your Drupal Commerce Web Shop to provide a separate payment option to pay using ViaBill.

#Requirements
------------
* PHP >= 5.2.0

#Compatibility
-------------
* Drupal 7.0
* Drupal Commerce 7.0 
* Compatible with CommerceKickstart


###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 

2. Module contains one folder a.) commerce_viabillepay

3. Extract the folder uc_viabillepay and place it inside  DrupalProject/modules

4. Admin Panel- Choose Modules in the menu . Find and check both ViaBill ePay modules in COMMERCE (ViaBill ePay), and click on the button ‘Save configuration’ at the bottom.

5. Go to Store Settings -> Advanced Store Settings -> Payment methods.

6. Activate the module by pressing ‘Enable’.Click on ‘Edit’ by the ViaBill ePay module.

7. Find ViaBill ePay under Actions, and click on ‘Edit’ .

8. Enter your merchant number in the field “merchant number”. It’s either a test or a production merchant number. 

9. Enter the Price Tag Script received from ViaBill. 

10. Press ‘Save configuration’ at the bottom.

11. Done.


##Uninstallation/Disable Module
-----------------------
1. Go to the Admin->Modules
2. Look For "ViaBill ePay "
3. Uncheck the Payment Method and click Save Configuration.
4. Go to Admin->Modules->Uninstall
5. Select ViaBillePay module and click Uninstall.
6. Now we must delete the files physically from the folder they are stored in using FTP, SSH or your cPanel.


#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
